# DOCKER

## Architecture

![Refactoring1](docs/img/architecture.png)

## Development

```bash
$ git clone git@bitbucket.org:genomicepidemiology/phenex.git --recursive
$ cd phenex
$ docker-compose -f composer-development.yml up
```

## Redis - Client

```bash
$ docker exec -it phenex_redis_dev bash

$ redis-cli ping
PONG

$ redis-cli
$ redis-cli SET test 'test'
$ redis-cli GET test
```

## Mongo - MongoDB shell

```bash
$ docker exec -it phenex_mongo_dev bash

$ mongo localhost:27017
```


# KUBERNETES

## Setup project inside vagrant cluster

### Executed on master node
```bash
$ cd /nfs/www/
$ git clone https://ludd@bitbucket.org/genomicepidemiology/phenex.git
$ cd phenex
$ git submodule update --init --recursive
$ git submodule foreach git pull origin master
$ touch api/.env.secrets admin/.env.secrets
```

### Executed on minion nodes
```bash
# Build production images
$ cd /mnt/nfs/www/phenex/
$ docker-compose -f composer-testing.yml build
$ docker-compose -f composer-production.yml build
```

### Executed on master node
```bash
# Obtain ip
$ hostname -i
192.168.123.130

$ cd /nfs/www/phenex/
$ ./kubernetes/setup/setup.sh
$ kubectl apply -f kubernetes/setup
$ kubectl apply -f kubernetes/testing
$ kubectl apply -f kubernetes/production

# Get deployments
$ kubectl get deploy
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
api        2/2     2            2           119s
admin      2/2     2            2           119s
web        1/1     1            1           119s

# Get services
$ kubectl get svc
NAME       TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
api        LoadBalancer   10.105.242.24    <pending>     80:30901/TCP   5s
admin      LoadBalancer   10.104.160.225   <pending>     80:30903/TCP   5s
web        LoadBalancer   10.105.111.106   <pending>     80:30900/TCP   5s

# Get pods
$ kubectl get po -o wide
NAME                       READY   STATUS    RESTARTS   AGE     IP           NODE        NOMINATED NODE   READINESS GATES
api-7bcdc6589f-btvjb       1/1     Running   0          3m30s   10.44.0.15   web01       <none>           <none>
api-7bcdc6589f-flxwj       1/1     Running   0          3m30s   10.36.0.19   compute01   <none>           <none>
admin-7b894d7454-2f6vn     1/1     Running   0          3m30s   10.36.0.18   compute01   <none>           <none>
admin-7b894d7454-qlr6f     1/1     Running   0          3m30s   10.44.0.14   web01       <none>           <none>
web-5787b9645-z6rnj        1/1     Running   0          3m30s   10.44.0.13   web01       <none>           <none>
```

# Testing

### Executed on master node
```bash
# Execute this on any kubernetes node
$ curl 10.105.242.24:80/api/v1/demo
$ curl 10.104.160.225:80/admin
$ curl 10.105.111.106:80/

# Execute this in your browser
$ curl 192.168.123.13[0-2]:30901/api/v1/demo
$ curl 192.168.123.13[0-2]:30903/admin
$ curl 192.168.123.13[0-2]:30900/
```

### Context
```bash
# Inspect context
$ kubectl config --help
$ kubectl config get-contexts
$ kubectl config current-context
$ kubectl config view
$ kubectl config set-context phenex-admin@kubernetes --user=kubernetes-admin --cluster=kubernetes --namespace=phenex
$ kubectl config use-context phenex-admin@kubernetes
```

### Deployments
```bash
# Create deployment
$ kubectl run api --image=genomicepidemiology/phenex-api:prod --image-pull-policy=Never
$ kubectl run admin --image=genomicepidemiology/phenex-backend:prod --image-pull-policy=Never
$ kubectl run web --image=genomicepidemiology/phenex-frontend:prod --image-pull-policy=Never
```

### Services
```bash
# Create service
$ kubectl expose deployment api --type=LoadBalancer --name=api --port=80
$ kubectl expose deployment admin --type=LoadBalancer --name=admin --port=80
$ kubectl expose deployment web --type=LoadBalancer --name=web --port=80
```


# Project Size

```bash
# Admin
{
  find ./admin/app -name '*.py' | xargs wc -l | grep total;
  find ./admin/app -name '*.html' | xargs wc -l | grep total;
  find ./admin/config -name '*.ini' | xargs wc -l | grep total;
  find ./admin/docker -name 'Dockerfile.*' | xargs wc -l | grep total;
  find ./admin/docs -name '*.md' | xargs wc -l | grep total;
  find ./admin/scripts -name '*.sh' | xargs wc -l | grep total;
  find ./admin/scripts -name '*.js' | xargs wc -l | grep total;
  find ./admin/tests -name '*.py' | xargs wc -l | grep total;
  find ./admin/tests -name '*.sh' | xargs wc -l | grep total;
  find ./admin -type d \( -path ./admin/storage \) -prune -o -name 'composer-*.yml' | xargs wc -l | grep total;
  find ./admin -type d \( -path ./admin/storage \) -prune -o -name 'bitbucket-pipelines.yml' | xargs wc -l | grep total;
  find ./admin -type d \( -path ./admin/storage \) -prune -o -name 'requirements.txt' | xargs wc -l | grep total;
} | awk '{s+=$1} END {print s}'

# Api
{
  find ./api/app -name '*.py' | xargs wc -l | grep total;
  find ./api/app -name '*.html' | xargs wc -l | grep total;
  find ./api/config -name '*.ini' | xargs wc -l | grep total;
  find ./api/docker -name 'Dockerfile.*' | xargs wc -l | grep total;
  find ./api/docs -name '*.md' | xargs wc -l | grep total;
  find ./api/scripts -name '*.sh' | xargs wc -l | grep total;
  find ./api/scripts -name '*.js' | xargs wc -l | grep total;
  find ./api/tests -name '*.py' | xargs wc -l | grep total;
  find ./api/tests -name '*.sh' | xargs wc -l | grep total;
  find ./api -type d \( -path ./api/storage \) -prune -o -name 'composer-*.yml' | xargs wc -l | grep total;
  find ./api -type d \( -path ./api/storage \) -prune -o -name 'bitbucket-pipelines.yml' | xargs wc -l | grep total;
  find ./api -type d \( -path ./api/storage \) -prune -o -name 'requirements.txt' | xargs wc -l | grep total;
} | awk '{s+=$1} END {print s}'

# Webapp
{
  find ./web/app -name '*.js' | xargs wc -l | grep total;
  find ./web/app -name '*.scss' | xargs wc -l | grep total;
  find ./web/app -name '*.html' | xargs wc -l | grep total;
  find ./web/config -name '*.conf' | xargs wc -l | grep total;
  find ./web/docker -name 'Dockerfile.*' | xargs wc -l | grep total;
  find ./web/docs -name '*.md' | xargs wc -l | grep total;
  find ./web/scripts -name '*.sh' | xargs wc -l | grep total;
  find ./web/scripts -name '*.js' | xargs wc -l | grep total;
  find ./web/tests -name '*.sh' | xargs wc -l | grep total;
  find ./web -type d \( -path ./web/node_modules \) -prune -o -name 'composer-*.yml' | xargs wc -l | grep total;
  find ./web -type d \( -path ./web/node_modules \) -prune -o -name 'bitbucket-pipelines.yml' | xargs wc -l | grep total;
  find ./web -type d \( -path ./web/node_modules \) -prune -o -name 'package.json' | xargs wc -l   | grep total;
} | awk '{s+=$1} END {print s}'

# Phenex
{
  find . -maxdepth 1 -name 'composer-*.yml' | xargs wc -l | grep total;
  find . -maxdepth 1 -name '*.md' | xargs wc -l | grep total;
  find ./proxy -name '*.conf' | xargs wc -l | grep total;
  find kubernetes/ -name '*.yaml' | xargs wc -l | grep total;
} | awk '{s+=$1} END {print s}'
```
