# Directories

```bash
# testing
$ mkdir -p /nfs/data/kubeapps/genomicepidemiology/cge/production/permastore/redis
$ mkdir -p /nfs/data/kubeapps/genomicepidemiology/cge/production/permastore/mongodb
$ mkdir -p /nfs/data/kubeapps/genomicepidemiology/cge/production/permastore/rabbitmq
$ mkdir -p /nfs/data/kubeapps/genomicepidemiology/cge/production/permastore/data
```

# Secrets

TODO
```bash
```

# Cron

Backup cronjob
```bash
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin

# Backups
* * * * * kubectl exec -n genomicepidemiology $(kubectl get pod -n genomicepidemiology | grep cge-mongodb | cut -f1 -d" ") -- sh -c 'exec mongodump -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --archive' > /nfs/data/kubeapps/genomicepidemiology/cge/production/permastore/mongodb/backup/$(date +\%s).alldbs.archive
```
