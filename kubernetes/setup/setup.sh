#!/usr/bin/env bash

echo "Creating permastore directories"

mkdir -p /nfs/data/kubeapps/genomicepidemiology/phenex/production/permastore/redis
mkdir -p /nfs/data/kubeapps/genomicepidemiology/phenex/production/permastore/mongo

kubectl create namespace genomicepidemiology
kubectl config set-context genomicepidemiology-admin@kubernetes --user=kubernetes-admin --cluster=kubernetes --namespace=genomicepidemiology
kubectl config use-context genomicepidemiology-admin@kubernetes
