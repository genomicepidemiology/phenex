#!/usr/bin/env python3

import argparse
import json
import os
import pika
import re
import socket
import subprocess
import sys
import time
from datetime import datetime


def json_to_dict(json_str):
    """
    Conver JSON string to dictionary.
    """
    return json.loads(json_str)


def dict_to_json(dictionary):
    """
    Conver dictionary to JSON.
    """
    return json.dumps(dictionary)


def dict_to_args_str(dictionary):
    """
    Transform dictionary into string of CLI arguments.
    """
    args_str = ''
    for key, val in dictionary.items():
        if type(val) == bool:
            val = None

        if type(val) == list:
            val = str.join(',', val)

        if val is None:
            args_str += f'--{key} '
        else:
            args_str += f'--{key} {val} '

    return args_str.strip()


parser = argparse.ArgumentParser(description='RabbitMQ queue producer.')
parser.add_argument('-e', '--exchange', default=os.environ.get('SERVICE_NAME', 'ResFinder'), help='Name of rabbitmq exchagne where the messages will be send to.')
parser.add_argument('-q', '--queue', default=os.environ.get('SERVICE_VERSION', 'v3'), help='Name of rabbitmq queue where the messages will be send to.')
args = parser.parse_args()

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))

# receive sample message
input_channel = connection.channel()
input_channel.exchange_declare(exchange=args.exchange, exchange_type='direct')
input_channel.queue_declare(args.queue, durable=True, exclusive=False)
input_channel.queue_bind(exchange=args.exchange, queue=args.queue)

# send results message
RABBITMQ_OUTPUT_QUEUE = 'results'
output_channel = connection.channel()
output_channel.queue_declare(queue=RABBITMQ_OUTPUT_QUEUE, durable=True, exclusive=False)

print(f'[*] Host: "{socket.gethostname()} is listening for "{args.exchange}" exchange and is waiting for messages from "{args.queue}" queue . To exit press CTRL+C.')
print(f'[*] Results will be send directly to : "{RABBITMQ_OUTPUT_QUEUE}" queue.')


# resfinder --inputfile /app/tests/test.fsa --methodPath /bin/blastn --threshold 0.90 --min_cov 0.60 --databasePath /app/db --outputPath /tmp/resfinder/results --databases aminoglycoside,beta-lactam -x
def run_sample(ch, method, properties, message):
    print(f'[x] Host: {socket.gethostname()} received corr_id: {properties.correlation_id}, Message: {message}')

    # Normalizing message
    msg = json_to_dict(message)
    args_str = dict_to_args_str(msg['options'])

    # Normalizing files
    normalized_files = [file.replace('/src/storage', '/app') for file in msg['files']]
    input_files = str.join(' ', normalized_files)

    # Select default method
    methodPath = '/bin/kma'
    if isFastaFile(msg['files']):
        methodPath = '/bin/blastn'

    results_msg = {
        'error': '',
        'recipient': msg['recipient'],
        'service': {
            'name': args.exchange,
            'version': args.queue
        },
        'metadata': {
            'sample_id': msg['sample_id'],
            'correlation_id': properties.correlation_id,
            'filesname': msg['filesname']
        }
    }

    # Create output directory
    try:
        output_dir = time.strftime(f'/app/data/analysis/%Y/%m/%d/{properties.correlation_id}')
        os.makedirs(output_dir, exist_ok=True)
        results_msg['metadata']['output_dir'] = output_dir
    except Exception as e:
        results_msg['error'] = str(e)

    # Construct CLI command
    command = f'resfinder {args_str} '
    command += f'--inputfile {input_files} '
    command += f'--outputPath {output_dir} '
    command += f'--methodPath {methodPath} '
    command += '--databasePath /app/db'
    # print('>> COMMAND:', command)

    results_msg['metadata']['command'] = command

    # Executing CLI command and create result message
    try:
        start_date = datetime.now()
        result = subprocess.run(command.split(' '), capture_output=True)
        delta_date = datetime.now() - start_date
        results_msg['metadata']['run_date'] = start_date.strftime('%d-%m-%Y %H:%M:%S')
        results_msg['metadata']['run_time'] = delta_date.total_seconds()
    except Exception as e:
        results_msg['error'] = str(e)

    results_msg['raw_output'] = result.stdout.decode('utf-8')
    if results_msg['raw_output'] == '':
        results_msg['error'] = result.stderr.decode('utf-8')

    ch.basic_ack(delivery_tag=method.delivery_tag)

    # Convert stdout to JSON, if not successful read output from first *.json file
    results_msg['json_output'] = ''
    try:
        results_msg['json_output'] = json.dumps(json.loads(output))
    except Exception:
        results_msg['json_output'] = load_json_from_dir(output_dir)

    results_msg_json = dict_to_json(results_msg)

    send_run_results(results_msg_json, properties.correlation_id)


def isFastaFile(files):
    for file in files:
        if file.endswith('gz'):
            return re.search('(\.fasta.gz)$|(\.fa.gz)$|(\.fsa.gz)$|(\.fna.gz)$', file) is not None
        return re.search('(\.fasta)$|(\.fa)$|(\.fsa)$|(\.fna)$', file) is not None


def load_json_from_dir(json_dir):
    try:
        files = os.listdir(json_dir)
        josn_file = [file for file in files if file.endswith('.json')]
        if josn_file:
            josn_file_path = f'{json_dir}/{josn_file[0]}'
            with open(josn_file_path) as jf:
                return json.loads(jf.read())
    except Exception:
        return ''


def send_run_results(message, corr_id):
    print(f'[x] Host: {socket.gethostname()}, sending result mesage: {message} to {RABBITMQ_OUTPUT_QUEUE} queue.')
    output_channel.basic_publish(exchange='', routing_key=RABBITMQ_OUTPUT_QUEUE, body=message, properties=pika.BasicProperties(
        delivery_mode=2,
        content_type='application/json',
        correlation_id=corr_id
    ))


input_channel.basic_qos(prefetch_count=1)
input_channel.basic_consume(queue=args.queue, auto_ack=False, on_message_callback=run_sample)
input_channel.start_consuming()
