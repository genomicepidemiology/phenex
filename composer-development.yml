version: "3"
services:
  proxy:
    image: nginx:1.15.3
    container_name: phenex_dev_proxy
    hostname: proxy-dev
    networks:
      frontend:
        ipv4_address: 172.21.0.5
    depends_on:
      - api
      - frontend
      - backend
      - resfinder_frontend
    ports:
      - "801:80"
      - "4431:443"
    volumes:
      - ./proxy/nginx.conf:/etc/nginx/nginx.conf
      - ./proxy/proxy.nginx.conf:/etc/nginx/conf.d/default.conf

  rabbitmq:
    image: rabbitmq:3.8.2-management
    container_name: phenex_dev_rabbitmq
    hostname: rabbitmq-dev
    networks:
      backend:
        ipv4_address: 172.22.0.5
    ports:
      - 5672:5672
      - 15672:15672

  redis:
    networks:
      backend:
        ipv4_address: 172.22.0.6
    image: redis:5.0.4
    restart: always
    container_name: phenex_dev_redis
    hostname: redis-dev
    environment:
      TZ: Europe/Copenhagen
    volumes:
      - ./db/redis/:/data

  mongo:
    networks:
      backend:
        ipv4_address: 172.22.0.7
    ports:
      - "25000:27017"
    image: mongo:4.1.10
    restart: always
    container_name: phenex_dev_mongo
    hostname: mongo-dev
    env_file: db/.env.secrets
    environment:
      TZ: Europe/Copenhagen
    volumes:
      - ./db/mongo/:/data/db
      - ./db/mongo-init.js:/docker-entrypoint-initdb.d/mongo-init.js:ro

  chromedriver:
    networks:
      backend:
        ipv4_address: 172.22.0.8
    image: robcherry/docker-chromedriver
    restart: always
    privileged: true
    container_name: phenex_dev_chromedriver
    environment:
      - CHROMEDRIVER_WHITELISTED_IPS=''

  resfinder_frontend:
    networks:
      frontend:
        ipv4_address: 172.21.0.6
    ports:
      - "8090:8090"
    image: genomicepidemiology/phenex-resfinder-frontend:dev
    restart: always
    container_name: phenex_dev_resfinder_frontend
    hostname: resfinder-frontend-dev
    build:
      context: resfinder-frontend/
      dockerfile: docker/Dockerfile.development
    command: sh -c "/app/scripts/startup.sh"
    volumes:
      - ./resfinder-frontend/:/app
    environment:
      VUE_APP_AUTH_URL: "//phenex.docker"

  resfinder_backend:
    image: genomicepidemiology/resfinder:v3-kubernetes
    restart: always
    networks:
      backend:
    container_name: phenex_dev_resfinder_backend
    hostname: resfinder-backend-dev
    build:
      context: resfinder-backend/
      dockerfile: docker/Dockerfile
    depends_on:
      - rabbitmq
    volumes:
      - ./api/storage/data/uploads:/app/data/uploads
      - ./api/storage/data/analysis:/app/data/analysis
      - ./queue/resfinder.v3.consumer.py:/app/src/resfinder.v3.consumer.py
    environment:
      PYTHONUNBUFFERED: 'True'
      PYTHONIOENCODING: UTF-8
      SERVICE_NAME: ResFinder
      SERVICE_VERSION: v3
    command: python3 /app/src/resfinder.v3.consumer.py
    # command: /bin/sleep 36000

  frontend:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.7
    ports:
      - "9003:8080"
    image: genomicepidemiology/phenex-frontend:dev
    restart: always
    container_name: phenex_dev_frontend
    hostname: frontend-dev
    build:
      context: frontend/
      dockerfile: docker/Dockerfile.development
      args:
        DEPLOYMENT: development
    volumes:
      - ./frontend/:/app
    environment:
      HOST: phenex.docker
      DEBUG: "true"
      DEPLOYMENT: development
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8
    command: sh -c "/app/scripts/startup.sh"

  backend:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.8
    ports:
      - "9008:80"
    image: genomicepidemiology/phenex-backend:dev
    restart: always
    container_name: phenex_dev_backend
    hostname: backend-dev
    build:
      context: backend/
      dockerfile: docker/Dockerfile.development
      args:
        DEPLOYMENT: development
    volumes:
      - ./backend/:/src
    depends_on:
      - redis
      - mongo
    env_file: backend/.env.secrets
    environment:
      HOST: phenex.docker
      DEBUG: "true"
      DEPLOYMENT: development
      MONGO_HOST: mongo
      MONGO_PORT: 27017
      MONGO_DB_NAME: phenex_dev
      TESTING_EMAIL: ludd@food.dtu.dk
      TESTING_PASSWORD: 123456
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8
      MIN_CODE_COVERAGE: 85

  api:
    networks:
      backend:
      frontend:
        ipv4_address: 172.21.0.9
    image: genomicepidemiology/phenex-api:dev
    restart: always
    container_name: phenex_dev_api
    hostname: api-dev
    build:
      context: api/
      dockerfile: docker/Dockerfile.development
    volumes:
      - ./api/:/src
      - ./api/storage/data:/src/storage/data
    depends_on:
      - redis
      - mongo
    env_file: api/.env.secrets
    environment:
      HOST: phenex.docker
      DEBUG: "true"
      DEPLOYMENT: development
      TZ: Europe/Copenhagen
      LANG: C.UTF-8
      LC_ALL: C.UTF-8
      MONGO_HOST: mongo
      MONGO_PORT: 27017
      MONGO_DB_NAME: phenex_dev
      REDIS_HOST: redis
      REDIS_PORT: 6379
      MAIL_SERVER: smtp.mailgun.org
      MAIL_PORT: 587
      MAIL_SENDER: noreply@genomicepidemiology.io
      TESTING_EMAIL: ludd@food.dtu.dk
      TESTING_PASSWORD: 123456
      MIN_CODE_COVERAGE: 85
      PYTHONUNBUFFERED: "True"
      PYTHONIOENCODING: UTF-8

networks:
  frontend:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.21.0.0/24
  backend:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.22.0.0/24
